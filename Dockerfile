from bitnami/matomo:5.0.3
USER root
RUN apt update \
  && apt -y install wget
RUN mkdir -p /opt/bitnami/matomo/plugins/LoginOIDC
RUN wget https://github.com/dominik-th/matomo-plugin-LoginOIDC/archive/refs/tags/5.0.0.tar.gz
RUN tar -zvxf 5.0.0.tar.gz
RUN mv ./matomo-plugin-LoginOIDC-5.0.0/* /opt/bitnami/matomo/plugins/LoginOIDC/
RUN chown daemon:root /opt/bitnami/matomo/plugins/LoginOIDC/
RUN chmod -R 775 /opt/bitnami/matomo/plugins/LoginOIDC
RUN rm -rf matomo-plugin-LoginOIDC-5.0.0 5.0.0.tar.gz
RUN apt -y remove wget \
  && apt -y autoremove
